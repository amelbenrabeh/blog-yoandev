<?php

namespace App\DataFixtures;

use App\Entity\AboutXp;
use App\Entity\BlogConfig;
use App\Entity\BlogPosts;
use App\Entity\Categories;
use App\Entity\Comments;
use App\Entity\Tags;
use App\Entity\Users;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Ottaviano\Faker\Gravatar;
use Symfony\Component\Validator\Constraints\Date;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $faker->addProvider(new Gravatar($faker));

        for ($i = 0; $i < 20; $i++) {
            $tag = new Tags();
            $tag->setTitle($faker->word());
            $manager->persist($tag);

            $category = new Categories();
            $category->setTitle($faker->words(3, true));
            $manager->persist($category);

            $user = new Users();
            $user->setEmail($faker->email());
            $user->setFirstname($faker->firstName());
            $user->setLastname($faker->lastName());
            $user->setBio($faker->sentence(40, true));
            $user->setAvatar('avatar_defaut.png');
            $user->setPhone($faker->randomNumber());
            $user->setCreateAt(new DateTime());
            $user->setUpdateAt(new DateTime());
            $user->setPassword($this->encoder->encodePassword($user, $faker->password()));
            $manager->persist($user);

            $blogpost = new BlogPosts();
            $blogpost->setTitle($faker->words(4, true));
            $blogpost->setContent($faker->paragraphs(100, true));
            $blogpost->addTag($tag);
            $blogpost->setAuthor($user);
            $blogpost->addCategory($category);
            $blogpost->setCreateAt(new DateTime());
            $blogpost->setUpdateAt(new DateTime());
            $blogpost->setPublished(true);
            $blogpost->setFeatureImage('default.jpg');
            $manager->persist($blogpost);

            $comment = new Comments();
            $comment->setAuthor($faker->userName());
            $comment->setCreateAt(new DateTime());
            $comment->setUpdateAt(new DateTime());
            $comment->setContent($faker->sentence(40, true));
            $comment->setPublished(true);
            $comment->setBlogPosts($blogpost);
            $comment->setEmail($faker->email());
            $manager->persist($comment);
        }

        $blogConfig = new BlogConfig();
        $blogConfig->setBlogName('BlogName');
        $blogConfig->setBlohHeroTitle('BlogName');
        $blogConfig->setHomeHeroText($faker->text(200));
        $blogConfig->setHomeHeroImage('config_default.png');
        $blogConfig->setAboutTitle($faker->words(3, true));
        $blogConfig->setAboutMiniText($faker->text(200));
        $blogConfig->setAboutTab1Title($faker->words(2, true));
        $blogConfig->setAbouTitleTab1Text($faker->text(200));
        $blogConfig->setAboutTab2Title($faker->words(2, true));
        $blogConfig->setAboutTab2Text($faker->text(200));
        $blogConfig->setAboutTab3Title($faker->words(2, true));
        $blogConfig->setAboutTab3Text($faker->text(200));
        $blogConfig->setAboutHeroTitle('A propos');
        $blogConfig->setAboutHeroImage('config_default.png');
        $blogConfig->setAboutImage('config_default.png');
        $blogConfig->setAboutVideoLink('https://www.youtube.com/watch?v=42WxpWKtlJg');
        $blogConfig->setContactHeroTitle('Me contacter');
        $blogConfig->setContactHeroImage('config_default.png');
        $blogConfig->setContactAdress($faker->address());
        $blogConfig->setContactPhone('0600000000');
        $blogConfig->setContactEmail($faker->email());
        $blogConfig->setContactWebsiteName('Website name');
        $blogConfig->setContactWebsiteLink($faker->url());
        $blogConfig->setContactTwitter('https://twitter.com/yOyO38');
        $blogConfig->setContactLinkedin('https://www.linkedin.com/in/yoan-bernabeu-392638116/');
        $blogConfig->setContactGitlab('https://gitlab.com/yoan.bernabeu');
        $blogConfig->setCreateAt(new DateTime());
        $manager->persist($blogConfig);

        for ($i = 0; $i < 5; $i++) {
            $aboutXp = new AboutXp();
            $aboutXp->setAboutXpText($faker->text(200));
            $aboutXp->setAboutXpTitle($faker->words(2, true));
            $aboutXp->setAboutXpCompagnyName('World Compagny');
            $aboutXp->setBlogConfig($blogConfig);
            $manager->persist($aboutXp);
        }

        $manager->flush();
    }
}
