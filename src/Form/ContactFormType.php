<?php

namespace App\Form;

use App\Entity\ContactForm;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['attr' => [
                'class'         => 'form-control',
                'placeholder'   => 'Votre nom'
                ]])

            ->add('email', EmailType::class, ['attr' => [
                'class'         => 'form-control',
                'placeholder'   => 'Votre adresse email'
                ]])

            ->add('subject', TextType::class, ['attr' => [
                'class'         => 'form-control',
                'placeholder'   => 'Objet'
                ]])

            ->add('message', TextareaType::class, ['attr' => [
                'class'         => 'form-control',
                'placeholder'   => 'Votre message',
                'cols'          => '30',
                'rows'          => '7'
                ]])

            ->add('save', SubmitType::class, ['label' => 'Envoyer le message', 'attr' => [
                'class'         => 'btn btn-primary py-3 px-5',
                ]])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ContactForm::class,
        ]);
    }
}
