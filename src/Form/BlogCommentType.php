<?php

namespace App\Form;

use App\Entity\Comments;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BlogCommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Author', TextType::class, ['attr' => [
                'class'         => 'form-control',
                ]])

            ->add('email', EmailType::class, ['attr' => [
                'class'         => 'form-control',
                ]])

            ->add('content', TextareaType::class, ['attr' => [
                'class'         => 'form-control',
                'cols'          => '30',
                'rows'          => '10'
                ]])

            ->add('save', SubmitType::class, [
                'label' => 'Envoyer le commentaire',
                'attr' => [
                    'class' => 'btn py-3 px-4 btn-primary',
                    ]
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Comments::class,
        ]);
    }
}
