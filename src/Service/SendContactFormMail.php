<?php

namespace App\Service;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class SendContactFormMail
{
    public function sendEmail(MailerInterface $mailer, $fromEmail, $fromName, $to, $subject, $text)
    {
        $email = (new Email())
                ->from(new Address($fromEmail, $fromName))
                ->to($to)
                ->subject($subject)
                ->text($text);
        $mailer->send($email);

        return true ;
    }
}
