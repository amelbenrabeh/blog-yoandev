<?php

namespace App\Service;

use ArrayObject;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class ExtractWordsForSearch
{
    public function extract($words)
    {
        /*
         * caractères que l'on va remplacer (tout ce qui sépare les mots, en fait)
         */
        $aremplacer = array(",",".",";",":","!","?","(",")","[","]","{","}","\"","'"," ");
     
        /*
         * ... on va les remplacer par un espace, il n'y aura donc plus dans $words
         * que des mots et des espaces
         */
        $enremplacement = " ";
     
          
        /*
         * on fait le remplacement (comme dit ci-avant), puis on supprime les espaces de
         * début et de fin de chaîne (trim)
         */
        $sansponctuation = trim(str_replace($aremplacer, $enremplacement, $words));
       
        /*
         * on coupe la chaîne en fonction d'un séparateur, et chaque élément est une
         * valeur d'un tableau
         */
        $separateur = "#[ ]+#"; // 1 ou plusieurs espaces
        $wordsArray = preg_split($separateur, $sansponctuation);
          
        return $wordsArray;
    }
}
