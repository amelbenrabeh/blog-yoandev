<?php

namespace App\Repository;

use App\Entity\BlogConfig;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BlogConfig|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogConfig|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogConfig[]    findAll()
 * @method BlogConfig[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogConfigRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BlogConfig::class);
    }

    // /**
    //  * @return BlogConfig[] Returns an array of BlogConfig objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BlogConfig
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
