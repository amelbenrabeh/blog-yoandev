<?php

namespace App\Repository;

use App\Entity\AboutXp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AboutXp|null find($id, $lockMode = null, $lockVersion = null)
 * @method AboutXp|null findOneBy(array $criteria, array $orderBy = null)
 * @method AboutXp[]    findAll()
 * @method AboutXp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AboutXpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AboutXp::class);
    }

    // /**
    //  * @return AboutXp[] Returns an array of AboutXp objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AboutXp
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
