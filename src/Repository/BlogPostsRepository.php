<?php

namespace App\Repository;

use App\Entity\BlogPosts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BlogPosts|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogPosts|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogPosts[]    findAll()
 * @method BlogPosts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogPostsRepository extends ServiceEntityRepository
{
    private $qb;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BlogPosts::class);
    }

  
    public function findTheTwoLastBlogPosts()
    {
        return $this->createQueryBuilder('b')
            ->orderBy('b.UpdateAt', 'DESC')
            ->setMaxResults(2)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findTheThreeLastBlogPosts()
    {
        return $this->createQueryBuilder('b')
            ->orderBy('b.UpdateAt', 'DESC')
            ->setMaxResults(3)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByContentLike($value)
    {
        $qb = $this->createQueryBuilder('posts');
        foreach ($value as &$post) {
            $qb->orWhere('posts.content LIKE :searchTerm')
            ->orWhere('posts.title LIKE :searchTerm');
        }

        $qb->setMaxResults(9);
        $qb->setParameter('searchTerm', '%'.$post.'%');

        return $qb->getQuery()->getResult();
    }
}
