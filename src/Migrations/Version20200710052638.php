<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200710052638 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE about_xp (id INT AUTO_INCREMENT NOT NULL, blog_config_id INT DEFAULT NULL, about_xp_text LONGTEXT NOT NULL, about_xp_title VARCHAR(255) NOT NULL, about_xp_compagny_name VARCHAR(255) NOT NULL, INDEX IDX_99842C3481A5EECC (blog_config_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blog_config (id INT AUTO_INCREMENT NOT NULL, blog_name VARCHAR(255) NOT NULL, bloh_hero_title VARCHAR(255) NOT NULL, home_hero_text LONGTEXT NOT NULL, home_hero_image VARCHAR(255) NOT NULL, about_title VARCHAR(255) NOT NULL, about_mini_text LONGTEXT NOT NULL, about_tab1_title VARCHAR(255) NOT NULL, abou_title_tab1_text LONGTEXT NOT NULL, about_tab2_title VARCHAR(255) NOT NULL, about_tab2_text LONGTEXT NOT NULL, about_tab3_title VARCHAR(255) NOT NULL, about_tab3_text LONGTEXT NOT NULL, about_hero_title VARCHAR(255) NOT NULL, about_hero_image VARCHAR(255) NOT NULL, about_image VARCHAR(255) NOT NULL, about_video_link VARCHAR(255) NOT NULL, contact_hero_title VARCHAR(255) NOT NULL, contact_hero_image VARCHAR(255) NOT NULL, contact_adress LONGTEXT NOT NULL, contact_phone INT NOT NULL, contact_email VARCHAR(255) NOT NULL, contact_website_name VARCHAR(255) NOT NULL, contact_website_link VARCHAR(255) NOT NULL, contact_twitter VARCHAR(255) NOT NULL, contact_linkedin VARCHAR(255) NOT NULL, contact_gitlab VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE about_xp ADD CONSTRAINT FK_99842C3481A5EECC FOREIGN KEY (blog_config_id) REFERENCES blog_config (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE about_xp DROP FOREIGN KEY FK_99842C3481A5EECC');
        $this->addSql('DROP TABLE about_xp');
        $this->addSql('DROP TABLE blog_config');
    }
}
