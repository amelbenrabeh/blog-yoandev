<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200708194800 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Création des entités';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE social_networks (id INT AUTO_INCREMENT NOT NULL, users_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, url VARCHAR(255) NOT NULL, INDEX IDX_5788200767B3B43D (users_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comments (id INT AUTO_INCREMENT NOT NULL, blog_posts_id INT DEFAULT NULL, author VARCHAR(255) NOT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, content VARCHAR(255) NOT NULL, published TINYINT(1) NOT NULL, INDEX IDX_5F9E962AA1BF7CE2 (blog_posts_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blog_posts (id INT AUTO_INCREMENT NOT NULL, author_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, content LONGTEXT NOT NULL, slug VARCHAR(255) NOT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, published TINYINT(1) NOT NULL, feature_image VARCHAR(255) NOT NULL, INDEX IDX_78B2F932F675F31B (author_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blog_posts_tags (blog_posts_id INT NOT NULL, tags_id INT NOT NULL, INDEX IDX_332A8F76A1BF7CE2 (blog_posts_id), INDEX IDX_332A8F768D7B4FB4 (tags_id), PRIMARY KEY(blog_posts_id, tags_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE blog_posts_categories (blog_posts_id INT NOT NULL, categories_id INT NOT NULL, INDEX IDX_DCC081A0A1BF7CE2 (blog_posts_id), INDEX IDX_DCC081A0A21214B7 (categories_id), PRIMARY KEY(blog_posts_id, categories_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tags (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, slug VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, bio LONGTEXT DEFAULT NULL, avatar VARCHAR(255) DEFAULT NULL, slug VARCHAR(255) NOT NULL, phone INT DEFAULT NULL, create_at DATETIME NOT NULL, update_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_1483A5E9E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE social_networks ADD CONSTRAINT FK_5788200767B3B43D FOREIGN KEY (users_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE comments ADD CONSTRAINT FK_5F9E962AA1BF7CE2 FOREIGN KEY (blog_posts_id) REFERENCES blog_posts (id)');
        $this->addSql('ALTER TABLE blog_posts ADD CONSTRAINT FK_78B2F932F675F31B FOREIGN KEY (author_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE blog_posts_tags ADD CONSTRAINT FK_332A8F76A1BF7CE2 FOREIGN KEY (blog_posts_id) REFERENCES blog_posts (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blog_posts_tags ADD CONSTRAINT FK_332A8F768D7B4FB4 FOREIGN KEY (tags_id) REFERENCES tags (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blog_posts_categories ADD CONSTRAINT FK_DCC081A0A1BF7CE2 FOREIGN KEY (blog_posts_id) REFERENCES blog_posts (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE blog_posts_categories ADD CONSTRAINT FK_DCC081A0A21214B7 FOREIGN KEY (categories_id) REFERENCES categories (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE blog_posts_categories DROP FOREIGN KEY FK_DCC081A0A21214B7');
        $this->addSql('ALTER TABLE comments DROP FOREIGN KEY FK_5F9E962AA1BF7CE2');
        $this->addSql('ALTER TABLE blog_posts_tags DROP FOREIGN KEY FK_332A8F76A1BF7CE2');
        $this->addSql('ALTER TABLE blog_posts_categories DROP FOREIGN KEY FK_DCC081A0A1BF7CE2');
        $this->addSql('ALTER TABLE blog_posts_tags DROP FOREIGN KEY FK_332A8F768D7B4FB4');
        $this->addSql('ALTER TABLE social_networks DROP FOREIGN KEY FK_5788200767B3B43D');
        $this->addSql('ALTER TABLE blog_posts DROP FOREIGN KEY FK_78B2F932F675F31B');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE social_networks');
        $this->addSql('DROP TABLE comments');
        $this->addSql('DROP TABLE blog_posts');
        $this->addSql('DROP TABLE blog_posts_tags');
        $this->addSql('DROP TABLE blog_posts_categories');
        $this->addSql('DROP TABLE tags');
        $this->addSql('DROP TABLE users');
    }
}
