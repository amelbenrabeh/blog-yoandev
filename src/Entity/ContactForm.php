<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class ContactForm
{
    /**
     * name
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=100)
     * @var string|null
     */
    private $name;

    /**
     * @Assert\NotBlank()
     * @Assert\Email()
     * @var string|null
     */
    private $email;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=100)
     * @var string|null
     */
    private $subject;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min=3)
     * @var string|null
     */
    private $message;

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(?string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }
}
