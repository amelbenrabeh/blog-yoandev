<?php

namespace App\Entity;

use App\Repository\BlogConfigRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=BlogConfigRepository::class)
 * @Vich\Uploadable
 */
class BlogConfig
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $blogName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $blohHeroTitle;

    /**
     * @ORM\Column(type="text")
     */
    private $homeHeroText;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $homeHeroImage;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="blogPosts", fileNameProperty="homeHeroImage")
     *
     * @var File|null
     */
    private $homeHeroImageFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $aboutTitle;

    /**
     * @ORM\Column(type="text")
     */
    private $aboutMiniText;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $aboutTab1Title;

    /**
     * @ORM\Column(type="text")
     */
    private $abouTitleTab1Text;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $aboutTab2Title;

    /**
     * @ORM\Column(type="text")
     */
    private $aboutTab2Text;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $aboutTab3Title;

    /**
     * @ORM\Column(type="text")
     */
    private $aboutTab3Text;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $aboutHeroTitle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $aboutHeroImage;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="blogPosts", fileNameProperty="aboutHeroImage")
     *
     * @var File|null
     */
    private $aboutHeroImageFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $aboutImage;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="blogPosts", fileNameProperty="aboutImage")
     *
     * @var File|null
     */
    private $aboutImageFile;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $aboutVideoLink;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contactHeroTitle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contactHeroImage;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="blogPosts", fileNameProperty="contactHeroImage")
     *
     * @var File|null
     */
    private $contactHeroImageFile;

    /**
     * @ORM\Column(type="text")
     */
    private $contactAdress;

    /**
     * @ORM\Column(type="integer")
     */
    private $contactPhone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contactEmail;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contactWebsiteName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contactWebsiteLink;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contactTwitter;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contactLinkedin;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contactGitlab;

    /**
     * @ORM\OneToMany(targetEntity=AboutXp::class, mappedBy="blogConfig")
     */
    private $aboutXp;

    /**
     * @ORM\Column(type="datetime")
     */
    private $CreateAt;

    public function __construct()
    {
        $this->aboutXp = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBlogName(): ?string
    {
        return $this->blogName;
    }

    public function setBlogName(string $blogName): self
    {
        $this->blogName = $blogName;

        return $this;
    }

    public function getBlohHeroTitle(): ?string
    {
        return $this->blohHeroTitle;
    }

    public function setBlohHeroTitle(string $blohHeroTitle): self
    {
        $this->blohHeroTitle = $blohHeroTitle;

        return $this;
    }

    public function getHomeHeroText(): ?string
    {
        return $this->homeHeroText;
    }

    public function setHomeHeroText(string $homeHeroText): self
    {
        $this->homeHeroText = $homeHeroText;

        return $this;
    }

    public function getHomeHeroImage(): ?string
    {
        return $this->homeHeroImage;
    }

    public function setHomeHeroImage($homeHeroImage): self
    {
        $this->homeHeroImage = $homeHeroImage;

        return $this;
    }

    public function getAboutTitle(): ?string
    {
        return $this->aboutTitle;
    }

    public function setAboutTitle(string $aboutTitle): self
    {
        $this->aboutTitle = $aboutTitle;

        return $this;
    }

    public function getAboutMiniText(): ?string
    {
        return $this->aboutMiniText;
    }

    public function setAboutMiniText(string $aboutMiniText): self
    {
        $this->aboutMiniText = $aboutMiniText;

        return $this;
    }

    public function getAboutTab1Title(): ?string
    {
        return $this->aboutTab1Title;
    }

    public function setAboutTab1Title(string $aboutTab1Title): self
    {
        $this->aboutTab1Title = $aboutTab1Title;

        return $this;
    }

    public function getAbouTitleTab1Text(): ?string
    {
        return $this->abouTitleTab1Text;
    }

    public function setAbouTitleTab1Text(string $abouTitleTab1Text): self
    {
        $this->abouTitleTab1Text = $abouTitleTab1Text;

        return $this;
    }

    public function getAboutTab2Title(): ?string
    {
        return $this->aboutTab2Title;
    }

    public function setAboutTab2Title(string $aboutTab2Title): self
    {
        $this->aboutTab2Title = $aboutTab2Title;

        return $this;
    }

    public function getAboutTab2Text(): ?string
    {
        return $this->aboutTab2Text;
    }

    public function setAboutTab2Text(string $aboutTab2Text): self
    {
        $this->aboutTab2Text = $aboutTab2Text;

        return $this;
    }

    public function getAboutTab3Title(): ?string
    {
        return $this->aboutTab3Title;
    }

    public function setAboutTab3Title(string $aboutTab3Title): self
    {
        $this->aboutTab3Title = $aboutTab3Title;

        return $this;
    }

    public function getAboutTab3Text(): ?string
    {
        return $this->aboutTab3Text;
    }

    public function setAboutTab3Text(string $aboutTab3Text): self
    {
        $this->aboutTab3Text = $aboutTab3Text;

        return $this;
    }

    public function getAboutHeroTitle(): ?string
    {
        return $this->aboutHeroTitle;
    }

    public function setAboutHeroTitle(string $aboutHeroTitle): self
    {
        $this->aboutHeroTitle = $aboutHeroTitle;

        return $this;
    }

    public function getAboutHeroImage(): ?string
    {
        return $this->aboutHeroImage;
    }

    public function setAboutHeroImage($aboutHeroImage): self
    {
        $this->aboutHeroImage = $aboutHeroImage;

        return $this;
    }

    public function getAboutImage(): ?string
    {
        return $this->aboutImage;
    }

    public function setAboutImage($aboutImage): self
    {
        $this->aboutImage = $aboutImage;

        return $this;
    }

    public function getAboutVideoLink(): ?string
    {
        return $this->aboutVideoLink;
    }

    public function setAboutVideoLink(string $aboutVideoLink): self
    {
        $this->aboutVideoLink = $aboutVideoLink;

        return $this;
    }

    public function getContactHeroTitle(): ?string
    {
        return $this->contactHeroTitle;
    }

    public function setContactHeroTitle(string $contactHeroTitle): self
    {
        $this->contactHeroTitle = $contactHeroTitle;

        return $this;
    }

    public function getContactHeroImage(): ?string
    {
        return $this->contactHeroImage;
    }

    public function setContactHeroImage($contactHeroImage): self
    {
        $this->contactHeroImage = $contactHeroImage;

        return $this;
    }

    public function getContactAdress(): ?string
    {
        return $this->contactAdress;
    }

    public function setContactAdress(string $contactAdress): self
    {
        $this->contactAdress = $contactAdress;

        return $this;
    }

    public function getContactPhone(): ?int
    {
        return $this->contactPhone;
    }

    public function setContactPhone(int $contactPhone): self
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(string $contactEmail): self
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    public function getContactWebsiteName(): ?string
    {
        return $this->contactWebsiteName;
    }

    public function setContactWebsiteName(string $contactWebsiteName): self
    {
        $this->contactWebsiteName = $contactWebsiteName;

        return $this;
    }

    public function getContactWebsiteLink(): ?string
    {
        return $this->contactWebsiteLink;
    }

    public function setContactWebsiteLink(string $contactWebsiteLink): self
    {
        $this->contactWebsiteLink = $contactWebsiteLink;

        return $this;
    }

    public function getContactTwitter(): ?string
    {
        return $this->contactTwitter;
    }

    public function setContactTwitter(string $contactTwitter): self
    {
        $this->contactTwitter = $contactTwitter;

        return $this;
    }

    public function getContactLinkedin(): ?string
    {
        return $this->contactLinkedin;
    }

    public function setContactLinkedin(string $contactLinkedin): self
    {
        $this->contactLinkedin = $contactLinkedin;

        return $this;
    }

    public function getContactGitlab(): ?string
    {
        return $this->contactGitlab;
    }

    public function setContactGitlab(string $contactGitlab): self
    {
        $this->contactGitlab = $contactGitlab;

        return $this;
    }

    /**
     * @return Collection|AboutXp[]
     */
    public function getAboutXp(): Collection
    {
        return $this->aboutXp;
    }

    public function addAboutXp(AboutXp $aboutXp): self
    {
        if (!$this->aboutXp->contains($aboutXp)) {
            $this->aboutXp[] = $aboutXp;
            $aboutXp->setBlogConfig($this);
        }

        return $this;
    }

    public function removeAboutXp(AboutXp $aboutXp): self
    {
        if ($this->aboutXp->contains($aboutXp)) {
            $this->aboutXp->removeElement($aboutXp);
            // set the owning side to null (unless already changed)
            if ($aboutXp->getBlogConfig() === $this) {
                $aboutXp->setBlogConfig(null);
            }
        }

        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setHomeHeroImageFile(?File $homeHeroImageFile = null): void
    {
        $this->homeHeroImageFile = $homeHeroImageFile;

        if (null !== $homeHeroImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updateAt = new \DateTimeImmutable();
        }
    }

    public function getHomeHeroImageFile(): ?File
    {
        return $this->homeHeroImageFile;
    }

    public function getCreateAt(): ?\DateTimeInterface
    {
        return $this->CreateAt;
    }

    public function setCreateAt(\DateTimeInterface $CreateAt): self
    {
        $this->CreateAt = $CreateAt;

        return $this;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setAboutHeroImageFile(?File $aboutHeroImageFile = null): void
    {
        $this->aboutHeroImageFile = $aboutHeroImageFile;

        if (null !== $aboutHeroImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updateAt = new \DateTimeImmutable();
        }
    }

    public function getAboutHeroImageFile(): ?File
    {
        return $this->aboutHeroImageFile;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setAboutImageFile(?File $aboutImageFile = null): void
    {
        $this->aboutImageFile = $aboutImageFile;

        if (null !== $aboutImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updateAt = new \DateTimeImmutable();
        }
    }

    public function getAboutImageFile(): ?File
    {
        return $this->aboutImageFile;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setContactHeroImageFile(?File $contactHeroImageFile = null): void
    {
        $this->contactHeroImageFile = $contactHeroImageFile;

        if (null !== $contactHeroImageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updateAt = new \DateTimeImmutable();
        }
    }

    public function getContactHeroImageFile(): ?File
    {
        return $this->contactHeroImageFile;
    }
}
