<?php

namespace App\Entity;

use App\Repository\AboutXpRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AboutXpRepository::class)
 */
class AboutXp
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $aboutXpText;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $aboutXpTitle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $aboutXpCompagnyName;

    /**
     * @ORM\ManyToOne(targetEntity=BlogConfig::class, inversedBy="aboutXp")
     */
    private $blogConfig;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAboutXpText(): ?string
    {
        return $this->aboutXpText;
    }

    public function setAboutXpText(string $aboutXpText): self
    {
        $this->aboutXpText = $aboutXpText;

        return $this;
    }

    public function getAboutXpTitle(): ?string
    {
        return $this->aboutXpTitle;
    }

    public function setAboutXpTitle(string $aboutXpTitle): self
    {
        $this->aboutXpTitle = $aboutXpTitle;

        return $this;
    }

    public function getAboutXpCompagnyName(): ?string
    {
        return $this->aboutXpCompagnyName;
    }

    public function setAboutXpCompagnyName(string $aboutXpCompagnyName): self
    {
        $this->aboutXpCompagnyName = $aboutXpCompagnyName;

        return $this;
    }

    public function getBlogConfig(): ?BlogConfig
    {
        return $this->blogConfig;
    }

    public function setBlogConfig(?BlogConfig $blogConfig): self
    {
        $this->blogConfig = $blogConfig;

        return $this;
    }

    public function __toString()
    {
        return $this->aboutXpCompagnyName. ' ' . $this->aboutXpTitle;
    }
}
