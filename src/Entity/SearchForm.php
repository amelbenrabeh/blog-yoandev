<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class SearchForm
{
    /**
     * name
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=100)
     * @var string|null
     */
    private $search;

    public function getSearch(): ?string
    {
        return $this->search;
    }

    public function setSearch(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
