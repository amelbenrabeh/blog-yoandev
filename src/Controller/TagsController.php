<?php

namespace App\Controller;

use App\Entity\Tags;
use App\Repository\BlogConfigRepository;
use App\Repository\BlogPostsRepository;
use App\Repository\TagsRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TagsController extends AbstractController
{
    private $blogConfigRepository;
    private $blogPostsRepository;

    public function __construct(BlogConfigRepository $blogConfigRepository, BlogPostsRepository $blogPostsRepository)
    {
        $this->blogConfigRepository = $blogConfigRepository;
        $this->blogPostsRepository =$blogPostsRepository;
    }

    /**
     * @Route("/tags/{Slug}/{page}", name="tags", requirements={"page"="\d+"})
     */
    public function index(Tags $tags, PaginatorInterface $paginator, TagsRepository $tagsRepository, Int $page = 1)
    {
        $blogConfig = $this->blogConfigRepository->findOneBy([]);
        $lastBlogPosts = $this->blogPostsRepository->findTheTwoLastBlogPosts();
        $data = $tagsRepository->findOneBy(['Slug' => $tags->getSlug()])->getBlogPosts();
        $blogPosts = $paginator->paginate(
            $data,
            $page,
            9
        );

        return $this->render('tags/index.html.twig', [
            'blogPosts'     => $blogPosts,
            'blogConfig'    => $blogConfig,
            'lastBlogPosts' => $lastBlogPosts,
            'tags'          => $tags,
        ]);
    }
}
