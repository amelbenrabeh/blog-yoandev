<?php

namespace App\Controller;

use App\Entity\ContactForm;
use App\Form\ContactFormType;
use App\Repository\BlogConfigRepository;
use App\Repository\BlogPostsRepository;
use App\Service\SendContactFormMail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    private $blogConfigRepository;
    private $blogPostsRepository;

    public function __construct(BlogConfigRepository $blogConfigRepository, BlogPostsRepository $blogPostsRepository)
    {
        $this->blogConfigRepository = $blogConfigRepository;
        $this->blogPostsRepository =$blogPostsRepository;
    }

    /**
     * @Route("/infos/contact", name="contact", methods="GET|POST")
     */
    public function index(Request $request, SendContactFormMail $sendContactFormMail, MailerInterface $mailer): Response
    {
        $blogConfig = $this->blogConfigRepository->findOneBy([]);
        $lastBlogPosts = $this->blogPostsRepository->findTheTwoLastBlogPosts();
        $contactForm = new ContactForm();

        $form = $this->createForm(ContactFormType::class, $contactForm);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contactForm = $form->getData();
            
            $sendContactFormMail->sendEmail(
                $mailer,
                $contactForm->getEmail(),
                $contactForm->getName(),
                $blogConfig->getContactEmail(),
                $contactForm->getSubject(),
                $contactForm->getMessage(),
            );
            
            $this->addFlash('success', 'Votre message est bien envoyé !');
            return $this->redirectToRoute('contact');
        }

        return $this->render('contact/contact.html.twig', [
            'blogConfig'    => $blogConfig,
            'lastBlogPosts' => $lastBlogPosts,
            'form'          => $form->createView(),
        ]);
    }
}
