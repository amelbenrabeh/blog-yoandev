<?php

namespace App\Controller\Admin;

use App\Entity\BlogConfig;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\FormField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class BlogConfigCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return BlogConfig::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            FormField::addPanel('Personalisation de la HOMEPAGE'),
            IdField::new('id')->onlyOnIndex(),
            TextField::new('blogName')
                ->setLabel('Nom du blog'),
            TextField::new('blohHeroTitle')
                ->setLabel('Titre en homepage'),
            TextareaField::new('homeHeroText')
                ->setLabel('Texte de la homepage'),
            ImageField::new('homeHeroImageFile')
                ->setFormType(VichImageType::class)
                ->setLabel('Image de la homepage')
                ->setBasePath('assets/upload/')
                ->hideOnIndex(),

            FormField::addPanel('Personalisation de la page ABOUT'),
            TextField::new('aboutHeroTitle')->hideOnIndex()
                ->setLabel('Nom de la page About'),
            ImageField::new('aboutHeroImageFile')
                ->setFormType(VichImageType::class)
                ->setLabel('Image de la page About')
                ->setBasePath('assets/upload/')
                ->hideOnIndex(),
            ImageField::new('aboutImageFile')
                ->setFormType(VichImageType::class)
                ->setLabel('Petite image de la page About')
                ->setBasePath('assets/upload/')
                ->hideOnIndex(),
            UrlField::new('aboutVideoLink')->hideOnIndex()
                ->setLabel('Lien vers une video Youtube/Vimeo'),
            TextField::new('aboutTitle')->hideOnIndex()
                ->setLabel('Titre au dessus des onglets'),
            TextField::new('aboutMiniText')->hideOnIndex()
                ->setLabel('Texte au dessus des onglets'),
            TextField::new('aboutTab1Title')->hideOnIndex()
                ->setLabel('Titre du premier onglet'),
            TextField::new('abouTitleTab1Text')->hideOnIndex()
                ->setLabel('Texte du premier onglet'),
            TextField::new('aboutTab2Title')->hideOnIndex()
                ->setLabel('Titre du deuxième onglet'),
            TextField::new('aboutTab2Text')->hideOnIndex()
                ->setLabel('Texte du deuxième onglet'),
            TextField::new('aboutTab3Title')->hideOnIndex()
                ->setLabel('Titre du troisième onglet'),
            TextField::new('aboutTab3Text')->hideOnIndex()
                ->setLabel('Texte du troisième onglet'),
            AssociationField::new('aboutXp')->hideOnIndex()
                ->setLabel('Expérience pro'),

            FormField::addPanel('Personalisation de la page CONTACT'),
            TextField::new('contactHeroTitle')->hideOnIndex()
                ->setLabel('Titre de la page Contact'),
            ImageField::new('contactHeroImageFile')
                ->setFormType(VichImageType::class)
                ->setLabel('Image de la page Contact')
                ->setBasePath('assets/upload/')
                ->hideOnIndex(),
            TextField::new('contactAdress')->hideOnIndex(),
            IntegerField::new('contactPhone')->hideOnIndex(),
            EmailField::new('contactEmail')->hideOnIndex(),
            TextField::new('contactWebsiteName')->hideOnIndex(),
            UrlField::new('contactWebsiteLink')->hideOnIndex(),
            UrlField::new('contactTwitter')->hideOnIndex(),
            UrlField::new('contactLinkedin')->hideOnIndex(),
            UrlField::new('contactGitlab')->hideOnIndex(),
        ];
    }
}
