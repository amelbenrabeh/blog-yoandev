<?php

namespace App\Controller\Admin;

use App\Entity\BlogPosts;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Vich\UploaderBundle\Mapping\Annotation\UploadableField;

class BlogPostsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return BlogPosts::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('title'),
            TextareaField::new('content')->setFormType(CKEditorType::class),
            AssociationField::new('Author'),
            AssociationField::new('Categories')->hideOnIndex(),
            AssociationField::new('tags')->hideOnIndex(),
            DateTimeField::new('CreateAt')->hideOnIndex(),
            DateTimeField::new('UpdateAt')->hideOnIndex(),
            BooleanField::new('Published'),
            ImageField::new('imageFile')
                ->setFormType(VichImageType::class)
                ->setLabel('Photo')
                ->setBasePath('assets/upload/')
                ->hideOnIndex(),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
        ;
    }
}
