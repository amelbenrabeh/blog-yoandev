<?php

namespace App\Controller\Admin;

use App\Entity\AboutXp;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class AboutXpCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return AboutXp::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
