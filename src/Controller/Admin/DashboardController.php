<?php

namespace App\Controller\Admin;

use App\Entity\AboutXp;
use App\Entity\BlogConfig;
use App\Entity\BlogPosts;
use App\Entity\Categories;
use App\Entity\Comments;
use App\Entity\SocialNetworks;
use App\Entity\Tags;
use App\Entity\Users;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('YoanDev');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('BlogPosts', 'icon class', BlogPosts::class);
        yield MenuItem::linkToCrud('Categories', 'icon class', Categories::class);
        yield MenuItem::linkToCrud('Tags', 'icon class', Tags::class);
        yield MenuItem::linkToCrud('Commentaires', 'icon class', Comments::class);
        yield MenuItem::linkToCrud('Utilisateurs', 'icon class', Users::class);
        yield MenuItem::linkToCrud('Exp. Pro', 'icon class', AboutXp::class);
        yield MenuItem::linkToCrud('Config Blog', 'icon class', BlogConfig::class);
    }
}
