<?php

namespace App\Controller;

use App\Entity\Categories;
use App\Repository\BlogConfigRepository;
use App\Repository\BlogPostsRepository;
use App\Repository\CategoriesRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CategoriesController extends AbstractController
{
    private $blogConfigRepository;
    private $blogPostsRepository;

    public function __construct(BlogConfigRepository $blogConfigRepository, BlogPostsRepository $blogPostsRepository)
    {
        $this->blogConfigRepository = $blogConfigRepository;
        $this->blogPostsRepository =$blogPostsRepository;
    }

    /**
     * @Route("/categories/{Slug}/{page}", name="categories", requirements={"page"="\d+"})
     */
    public function index(Categories $categories, PaginatorInterface $paginator, CategoriesRepository $categoriesRepository, Int $page = 1)
    {
        $blogConfig = $this->blogConfigRepository->findOneBy([]);
        $lastBlogPosts = $this->blogPostsRepository->findTheTwoLastBlogPosts();
        $data = $categoriesRepository->findOneBy(['Slug' => $categories->getSlug()])->getBlogPosts();
        $blogPosts = $paginator->paginate(
            $data,
            $page,
            9
        );

        return $this->render('categories/index.html.twig', [
            'blogPosts'     => $blogPosts,
            'blogConfig'    => $blogConfig,
            'lastBlogPosts' => $lastBlogPosts,
            'categories'    => $categories,
        ]);
    }
}
