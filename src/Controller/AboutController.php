<?php

namespace App\Controller;

use App\Repository\BlogConfigRepository;
use App\Repository\BlogPostsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AboutController extends AbstractController
{
    private $blogConfigRepository;
    private $blogPostsRepository;

    public function __construct(BlogConfigRepository $blogConfigRepository, BlogPostsRepository $blogPostsRepository)
    {
        $this->blogConfigRepository = $blogConfigRepository;
        $this->blogPostsRepository =$blogPostsRepository;
    }

    /**
     * @Route("/infos/about", name="about")
     */
    public function index()
    {
        $blogConfig = $this->blogConfigRepository->findOneBy([]);
        $lastBlogPosts = $this->blogPostsRepository->findTheTwoLastBlogPosts();

        return $this->render('about/about.html.twig', [
            'blogConfig' => $blogConfig,
            'lastBlogPosts' => $lastBlogPosts,
        ]);
    }
}
