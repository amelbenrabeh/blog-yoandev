<?php

namespace App\Controller;

use App\Entity\BlogPosts;
use App\Entity\Categories;
use App\Entity\Comments;
use App\Entity\SearchForm;
use App\Form\BlogCommentType;
use App\Form\SearchFormType;
use App\Service\ExtractWordsForSearch;
use App\Repository\BlogConfigRepository;
use App\Repository\BlogPostsRepository;
use App\Repository\CategoriesRepository;
use App\Repository\TagsRepository;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogPostsController extends AbstractController
{
    private $blogConfigRepository;
    private $blogPostsRepository;

    public function __construct(BlogConfigRepository $blogConfigRepository, BlogPostsRepository $blogPostsRepository)
    {
        $this->blogConfigRepository = $blogConfigRepository;
        $this->blogPostsRepository =$blogPostsRepository;
    }

    /**
     * @Route("/{page}", name="blog_posts_index", requirements={"page"="\d+"})
     */
    public function index(PaginatorInterface $paginator, Int $page = 1): Response
    {
        $blogConfig = $this->blogConfigRepository->findOneBy([]);
        $lastBlogPosts = $this->blogPostsRepository->findTheTwoLastBlogPosts();
        $data = $this->blogPostsRepository->findBy([], ['CreateAt' => 'desc']);

        $blogPosts = $paginator->paginate(
            $data,
            $page,
            5
        );

        return $this->render('blog_posts_index/index.html.twig', [
            'blogPosts' => $blogPosts,
            'blogConfig' => $blogConfig,
            'lastBlogPosts' => $lastBlogPosts,
        ]);
    }

    /**
     * @Route("/blog/{Slug}", name="blog_posts_show")
     */
    public function show(BlogPosts $blogpost, CategoriesRepository $categoriesRepository, TagsRepository $tagsRepository, Request $request, EntityManagerInterface $em): Response
    {
        $blogConfig = $this->blogConfigRepository->findOneBy([]);
        $lastBlogPosts = $this->blogPostsRepository->findTheTwoLastBlogPosts();
        $lastTrheeBlogPosts = $this->blogPostsRepository->findTheThreeLastBlogPosts();
        $categories = $categoriesRepository->findAll();
        $tags = $tagsRepository->findAll();
        
        $comment = new Comments();
        $comment->setBlogPosts($blogpost);
        $comment->setCreateAt(new DateTime());
        $comment->setUpdateAt(new DateTime());
        $comment->setPublished(true);

        $search = new SearchForm();

        $form = $this->createForm(BlogCommentType::class, $comment);
        $form->handleRequest($request);

        $search = $this->createForm(SearchFormType::class, $search);

        if ($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();
            $em->persist($comment);
            $em->flush();
            $this->addFlash('success', 'Commentaire bien envoyé, merci !');
            $this->redirectToRoute('blog_posts_show', ['Slug' => $blogpost->getSlug()]);
        }

        return $this->render('blog_posts_show/show.html.twig', [
            'blogConfig'            => $blogConfig,
            'lastBlogPosts'         => $lastBlogPosts,
            'lastThreeBlogPosts'    => $lastTrheeBlogPosts,
            'tags'                  => $tags,
            'categories'            => $categories,
            'singlePost'            => $blogpost,
            'form'                  => $form->createView(),
            'search'                => $search->createView(),
        ]);
    }

    /**
     * @Route("/blog{page}", name="blog_posts_blog", requirements={"page"="\d+"})
     */
    public function blog(PaginatorInterface $paginator, Int $page = 1): Response
    {
        $blogConfig = $this->blogConfigRepository->findOneBy([]);
        $lastBlogPosts = $this->blogPostsRepository->findTheTwoLastBlogPosts();
        $data = $this->blogPostsRepository->findBy([], ['CreateAt' => 'desc']);

        $blogPosts = $paginator->paginate(
            $data,
            $page,
            9
        );

        return $this->render('blog_post_blog/blog.html.twig', [
            'blogPosts' => $blogPosts,
            'blogConfig' => $blogConfig,
            'lastBlogPosts' => $lastBlogPosts,
        ]);
    }

    /**
     * @Route("/search/{page}", name="blog_posts_search", requirements={"page"="\d+"})
     */
    public function search(Request $request, PaginatorInterface $paginator, ExtractWordsForSearch $extract, Int $page = 1): Response
    {
        $blogConfig = $this->blogConfigRepository->findOneBy([]);
        $lastBlogPosts = $this->blogPostsRepository->findTheTwoLastBlogPosts();

        $search = $request->request->get('search_form')['search'];

        $wordsArray = $extract->extract($search);
        $data = $this->blogPostsRepository->findByContentLike($wordsArray);
        
        if (!$data) {
            $this->addFlash('error', 'Désolé nous n\'avons pas trouvé de résultats');
        }
      
        return $this->render('blog_posts_search/search.html.twig', [
            'blogPosts'     => $data,
            'blogConfig'    => $blogConfig,
            'lastBlogPosts' => $lastBlogPosts,
            'words'         => $search,
        ]);
    }
}
